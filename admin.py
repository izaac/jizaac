#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
admin.py

Created by Pradeep Gowda on 2008-05-04.
Copyright (c) 2008 Yashotech. All rights reserved.
Modified by Izaac Zavaleta, Pagination, memcache and other features
"""
import wsgiref.handlers

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import memcache
import sys
sys.path.insert(0, 'lib.zip')
from lib.utils import TehRequestHandler, administrator, Config
from blog import Entry

class AdminHandler(TehRequestHandler):
    @administrator
    def get(self):
        self.render("templates/admin.html")

class EntryListHandler(TehRequestHandler):
    @administrator
    def get(self):
        entries = memcache.get('entries_admin')
        if not entries:
            entries = Entry.all().order("-published")
            #set one day (86400 secs) expiration for 'entries_admin'
            memcache.add('entries_admin', entries, 86400) 
        self.render("templates/admin_entrylist.html",
                    entries=entries)
        #self.render("templates/admin_entrylist.html")

class ConfigHandler(TehRequestHandler):
    @administrator
    def get(self):
        self.render("templates/config.html")
    @administrator    
    def post(self):
        config = Config.all()
        config = config.fetch(1)[0]
        config.title = self.request.get("title")
        config.disqus = self.request.get("disqus")
        config.put()
        self.redirect('/')

