#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
webapp.py

Created by Pradeep Gowda on 2008-04-23.
Copyright (c) 2008 Yashotech. All rights reserved.
Modified by Izaac Zavaleta, Pagination, memcache and other features
"""
import wsgiref.handlers

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import memcache
from django.core.paginator import ObjectPaginator
import sys
sys.path.insert(0, 'lib.zip')
from lib.utils import Config

import os
import blog
import admin
      
def main():

    application = webapp.WSGIApplication([
        ("/*$", blog.HomePageHandler),
        (r"/page(\d+)/*$", blog.HomePageHandler),
        ("/login/*$", blog.LoginHandler),
        ("/logout/*$", blog.LogoutHandler),
        ("/entries/*$", blog.EntryIndexHandler),
        (r"/entries(\d+)/*$", blog.EntryIndexHandler),
        ("/feed/*$", blog.FeedHandler),
        ("/rss/*$", blog.RSSHandler),
        ("/entry/([^/]+)/*$", blog.EntryHandler),
        ("/entry/([^/]+)/edit/*$", blog.NewEntryHandler),
        ("/entry/([^/]+)/del/*$", blog.EntryDeleteHandler),
        (r'/date/(\d\d\d\d)/(\d\d)/*$', blog.EntriesForMonthHandler),
        ("/([^/]+)/edit/*$", blog.NewEntryHandler),
        ("/([^/]+)/del/*$", blog.EntryDeleteHandler),
        ("/topic/([^/]+)/*$", blog.TagHandler),
        ("/flush/*$", blog.FlushHandler),
        ("/admin/*$", admin.AdminHandler),
        ("/admin/new/*$", blog.NewEntryHandler),
        ("/admin/config/*$", admin.ConfigHandler),
        ("/admin/entrylist/*$", admin.EntryListHandler),

       # (r"/shooin/([^/]+)", shooin.ShooinHandler),
        (r"/robots.txt", blog.RobotHandler),
        (r"/sitemap.xml", blog.SitemapHandler),
        ("/([^/]+)/*$", blog.PageHandler),
        ], debug=True)
    
    config = Config.all()
    if config.count() > 0:
        config = config.fetch(1)[0]
    else: 
        config1 = Config(title="izaac log")
        config1.put()
       
    wsgiref.handlers.CGIHandler().run(application)

if __name__ == "__main__":
    main()
