#!/usr/bin/env python
# -*- encoding: utf-8 -*-
#TODO: Usar Pygments para Syntax Highlighting
"""
blog.py

Created by Pradeep Gowda on 2008-04-23.
Copyright (c) 2008 Yashotech. All rights reserved.
Modified by Izaac Zavaleta, Pagination, memcache and other features
"""
import wsgiref.handlers

from google.appengine.ext import webapp
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.api import users
from google.appengine.api import memcache
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError
import functools
import os
import sys
sys.path.insert(0, 'lib.zip')
from lib import utils, markdown2, BeautifulSoup, textile
from lib.utils import TehRequestHandler, administrator
from django.core.paginator import ObjectPaginator
import datetime
import xmlrpc
import xmlrpclib
import defs

class Entry(db.Model):
    author = db.UserProperty()
    title = db.StringProperty(required=True)
    slug = db.StringProperty(required=True)
    body = db.TextProperty(required=True)
    published = db.DateTimeProperty(auto_now_add=True)
    updated = db.DateTimeProperty(auto_now=True)
    markdown = db.TextProperty()
    body_html = db.TextProperty()
    excerpt = db.TextProperty()
    tags = db.ListProperty(db.Category)
    static = db.BooleanProperty()
    comments = db.BooleanProperty()
    
    def url(self):
        if self.static == False: return '/entry/%s' %  self.slug
        else: return '/%s' % self.slug

    def get_date(self):
        """date converter from UTC idea taken from studypy
        return (self.date +
                    datetime.timedelta(hours=8)).strftime(
                        "%a %b %d, %Y %H:%M:%S GMT+08:00")
        """   
        return self.published + datetime.timedelta(hours=defs.HOURS_FROM_UTC)

    def get_updated(self):
        return  self.updated + datetime.timedelta(hours=defs.HOURS_FROM_UTC)
    def get_updated_plain(self):
        return self.updated
    def get_rfc3339(self):
        return (self.updated + datetime.timedelta(hours=-6)).strftime('%Y-%m-%dT%H:%M:%SZ')
    def get_rfc3339_plain(self):
        return self.updated.strftime('%Y-%m-%dT%H:%M:%SZ')

    @classmethod
    def get_all_datetimes(cls):
        dates = {}
        articles = db.Query(Entry).filter('static =', False).order("-published").fetch(400)
        for article in articles:
            date = datetime.datetime(article.get_date().year,
                                     article.get_date().month,
                                     article.get_date().day)
            try:
                dates[date] += 1
            except KeyError:
                dates[date] = 1

        return dates

    @classmethod
    def published_query(cls):
        q = db.Query(Entry)
        q.filter('static = ', False)
        return q

    @classmethod
    def all_for_month(cls, year, month):
        start_date = datetime.date(year, month, 1)
        if start_date.month == 12:
            next_year = start_date.year + 1
            next_month = 1
        else:
            next_year = start_date.year
            next_month = start_date.month + 1

        end_date = datetime.date(next_year, next_month, 1)
        return Entry.published_query()\
                       .filter('published >=', start_date)\
                       .filter('published <', end_date)\
                       .order('-published')\
                       .fetch(400)

class DateCount(object):
    """
    Convenience class for storing and sorting year/month counts.
    """
    def __init__(self, date, count):
        self.date = date
        self.count = count

    def __cmp__(self, other):
        return cmp(self.date, other.date)

    def __hash__(self):
        return self.date.__hash__()

    def __str__(self):
        return '%s(%d)' % (self.date, self.count)

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, str(self))
    
class LoginHandler(TehRequestHandler):
    def get(self):
        user = users.get_current_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
        else:
            self.redirect('/')

class LogoutHandler(TehRequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            self.redirect(users.create_logout_url('/'))
        else:
            self.redirect('/')

class HomePageHandler(TehRequestHandler):
    def get(self, num=0):

        results = memcache.get('results')       
        paginate_by = 0
        paginator = None
        page = 0

        if results is None:
            entries = Entry.all()
            entries.filter("static =", False)
            results = entries.order('-published').fetch(limit=100)
            memcache.add('results', results, 86400) # 1 hour = 3600 this 1 day
            paginate_by = defs.NUMBER_OF_ITEMS_HOMEPAGE
            paginator = ObjectPaginator(results, paginate_by)
            try:
                #page = int(self.request.get('page',0))
                page = int(num)
                results = paginator.get_page(page)
            except:
                results = paginator.get_page(int(paginator.pages-1))
        else:
            paginate_by = defs.NUMBER_OF_ITEMS_HOMEPAGE
            paginator = ObjectPaginator(results, paginate_by)
            try:
                page = int(num)
                results = paginator.get_page(page)
            except:
                results = paginator.get_page(int(paginator.pages-1))
        
        date_list = get_month_counts()
        self.render("templates/home.html", 
                   entries=results, is_paginated=True,
                   results_per_page=paginate_by, has_next=paginator.has_next_page(page),
                   has_previous=paginator.has_previous_page(page), page=page+1,
                   next=page+1, previous=page-1, pages=range(paginator.pages),
                   date_list=date_list, date_path="/date"
                   )

class EntryIndexHandler(TehRequestHandler):
    def get(self, num=0):

        results = memcache.get('results')
        paginate_by = 0
        paginator = None
        page = 0

        if results is None:
            entries = Entry.all().filter("static =", False)
            entries.order('-published') 
            results = entries.fetch(limit=100) #fixed: implemented pagination
            memcache.add('results', results, 86400) # 300 secs = 5 mins
            paginate_by = 50
            paginator = ObjectPaginator(results, paginate_by)
            try:
                #page = int(self.request.get('page',0))
                page = int(num)
                results = paginator.get_page(page)
            except:
                results = paginator.get_page(int(paginator.pages-1))
        else:
            paginate_by = 50
            paginator = ObjectPaginator(results, paginate_by)
            try:
                #page = int(self.request.get('page',0))
                page = int(num)
                results = paginator.get_page(page)
            except:
                results = paginator.get_page(int(paginator.pages-1))           
        self.render("templates/entryindex.html",
                    entries=results, is_paginated=True,
                    results_per_page=paginate_by, has_next=paginator.has_next_page(page),
                    has_previous=paginator.has_previous_page(page), page=page+1,
                    next=page+1, previous=page-1, pages=range(paginator.pages)
                    )

class EntryHandler(TehRequestHandler):
    def get(self,slug):
        admin = users.is_current_user_admin()
        entry = db.Query(Entry).filter("slug =", slug).filter("static = ", False).get()      
        if not entry:
            self.error(404)
            self.render("templates/404.html")
            return
        self.render("templates/entry.html", entry=entry)

def get_month_counts():
    hash = Entry.get_all_datetimes()
    datetimes = hash.keys()
    date_count = {}
    for dt in datetimes:
        just_date = datetime.date(dt.year, dt.month, 1)
        try:
            date_count[just_date] += hash[dt]
        except KeyError:
            date_count[just_date] = hash[dt]

    dates = date_count.keys()
    dates.sort()
    dates.reverse()
    return [DateCount(date, date_count[date]) for date in dates]

class EntriesForMonthHandler(TehRequestHandler):

    def get(self, year, month):        
        if not month:
            self.error(404)
            self.render("templates/404.html")
            return
        if int(year) < 2009 or not (0 < int(month) < 12):
            self.error(404)
            self.render("templates/404.html")
            return            
        articles = Entry.all_for_month(int(year), int(month))      
        if not articles:
            self.error(404)
            self.render("templates/404.html")
            return
        date_list = get_month_counts()
        self.render("templates/entry_month.html", entries=articles, date_list=date_list,
                   date_path="/date", date="%s/%s"%(year,month)
                   )
        
class EntryDeleteHandler(TehRequestHandler):
        @administrator
        def get(self,slug):
            entry = db.Query(Entry).filter("slug =", slug).get()
            if not entry:
                self.error(404)
                self.render("templates/404.html")
                return
            self.render("templates/del.html", entry=entry)

        @administrator
        def post(self,slug):
            entry = db.Query(Entry).filter("slug =", slug).get()
            if not entry:
                self.error(404)
                self.render("templates/404.html")
                return
            delete = self.request.get("del")
            if delete and delete.upper() == 'Y':
                entry.delete()
            self.redirect('/entries')
                
class PageHandler(TehRequestHandler):
    def get(self, slug):
        admin = users.is_current_user_admin()
        entry = db.Query(Entry).filter("slug =", slug).filter("static =", True).get()
        if not entry:
            self.error(404)
            self.render("templates/404.html")
            return
        self.render("templates/page.html", entry=entry)

class TagHandler(TehRequestHandler):
    def get(self, slug):
        entries = Entry.all().filter("tags =", slug).fetch(limit=10)
        message = 'Entries belonging to tag `%s`' %(slug, )
        if not entries:
            self.error(404)
            self.render("templates/404.html")
            return
        self.render("templates/entryindex.html", entries=entries, message=message)
        

class FeedHandler(TehRequestHandler):
    def get(self):
        entries = memcache.get('entries')
        if not entries:

            entries = Entry.all().filter("static =", False).order('-published').fetch(limit=15)
            memcache.add('entries', entries, 86400)
            if not entries:
                entries = []
                entries.append(None) # same result using template as: {{entries.0.updated}}
            
        self.response.headers['Content-Type'] = 'application/atom+xml'
        self.render("templates/atom.xml", entries=entries)

class RSSHandler(TehRequestHandler):
    def get(self):
        """To add RSS 2.0 Feed"""
        entries = memcache.get('entries_rss')
        if not entries:

            entries = Entry.all().filter("static =", False).order('-published').fetch(limit=15)
            memcache.add('entries_rss', entries, 86400)
            if not entries:
                entries = []
                entries.append(None) # same result using template as: {{entries.0.updated}}
            
        self.response.headers['Content-Type'] = 'application/rss+xml'
        self.render("templates/rss20.rss", entries=entries)

class SitemapHandler(TehRequestHandler):
    def get(self):
        """To build sitemap.xml"""
        entries = memcache.get('entries_sitemap')
        if not entries:

            
            entries = Entry.all().filter("static =", False).order('-published').fetch(limit=100)
            memcache.add('entries_sitemap', entries, 86400)           
            if not entries:
                entries = []
                entries.append(None) # same result using template as: {{entries.0.updated}}

        self.response.headers['Content-Type'] = 'text/xml'
        self.render("templates/sitemap.xml", entries=entries)        

class RobotHandler(TehRequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        message = "User-agent: *\nDisallow: /admin\nDisallow: /admin/new"
        self.response.out.write(message)

def to_html(body,markdown):
    if markdown == 'markdown':
        body_html = markdown2.markdown(body)
    elif markdown == 'textile':
        body_html = textile.textile(body,)
    else:
        body_html = body
    return body_html

class FlushHandler(TehRequestHandler):
    @administrator
    def get(self):
        memcache.flush_all()
        self.redirect("/", permanent=True)
    

class NewEntryHandler(TehRequestHandler):
    @administrator
    def get(self,slug=None):
        if slug:
            entry = db.Query(Entry).filter("slug =", slug).get()
            if not entry:
                self.error(404)
                self.render("templates/404.html")
            if not entry.tags:
                self.render("templates/entry_edit.html", entry=entry)
            else:
                try:
                    tags = " ".join([tag for tag in entry.tags])
                except:
                    self.render("templates/entry_edit.html", entry=entry) 
                self.render("templates/entry_edit.html", entry=entry, tags=tags)
        else: self.render("templates/entry_edit.html")
        
    @administrator
    def post(self,slug=None):
        title = self.request.get("title")
        body = self.request.get("body")
        markdown = self.request.get("markup")
        st  = self.request.get("static")
        cm  = self.request.get("comments")
        if st == '1': static = True
        else: static = False
        if cm  == '1': comments = True
        else: comments = False
        
        tags = self.request.get("tags")
        tags = tags.split(' ')
        if len(tags) == 0:
            tags = ['general']
        tags = [db.Category(utils.slugify(tag)) for tag in tags if tag]

        body_html = to_html(body,markdown) #to_html() declared up in line 356

        soup = BeautifulSoup.BeautifulSoup(body_html, fromEncoding='utf-8')
        paras = soup.findAll('p')
        
        if paras:
            excerpt = paras[0].string
        else: excerpt = ''
        
        entry = db.Query(Entry).filter("slug =", slug).get()
        nuevo = False
        if not entry:
            entry = Entry(
                author=users.get_current_user(),
                title=title,
                slug=utils.slugify(title),
                body=body,
                body_html=body_html,
                markdown=markdown,
                excerpt=excerpt,
                tags=tags,
                static=static,
                comments=comments,
            )
            nuevo = True            
        else: 
            entry.title = title
            entry.body = body
            entry.body_html = body_html
            entry.excerpt = excerpt
            entry.static = static
            entry.tags = tags
            entry.comments = comments        
        memcache.flush_all() # flushes cache to display new entry
        try:
            entry.put()
        except CapabilityDisabledError:
            logging.debug('Caught CapabilityDisabledError')
            self.render("templates/error.html", error='CapabilityDisabledError')
        if nuevo: alert_the_media() #pings Ping-O-Matic
        self.redirect(entry.url())

def ping_pingomatic():

    url = defs.PING_URL
    import logging
    logging.debug('Pinging Ping-O-Matic at: %s' % url)
    try:
        transport = xmlrpc.GoogleXMLRPCTransport()
        rpc_server = xmlrpclib.ServerProxy(url, transport=transport)
        result = rpc_server.weblogUpdates.ping(defs.BLOG_NAME,
                                               defs.CANONICAL_BLOG_URL,
                                               defs.BLOG_FEED_URL
                                              )
        if result.get('flerror', False) == True:
            logging.error('Ping-O-Matic ping error from server: %s' %
                          result.get('message', '(No message in RPC result)'))
        else:
            logging.debug('Ping-O-Matic ping successful.')
    except:
        from google.appengine.api import urlfetch
        import sys
        raise urlfetch.DownloadError, \
              "Can't ping Ping-O-Matic: %s" % sys.exc_info()[1]

def alert_the_media():
    # Right now, we only alert pingomatic
    # we could add technorati and others
    ping_pingomatic()